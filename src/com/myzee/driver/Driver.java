package com.myzee.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.myzee.beans.HelloWorld;

public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = new ClassPathXmlApplicationContext("com/myzee/resources/spring.xml");
		HelloWorld h = (HelloWorld)context.getBean("hw");
		h.printMessage();
	}

}
